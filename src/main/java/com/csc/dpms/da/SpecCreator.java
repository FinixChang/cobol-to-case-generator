package com.csc.dpms.da;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.PositionInParagraph;
import org.apache.poi.xwpf.usermodel.TextSegement;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.apache.xmlbeans.XmlCursor;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRPr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.dpms.da.source.vo.GeneratorConfiguration;
import com.csc.dpms.da.source.vo.Program;
import com.csc.dpms.da.util.ParameterUtil;
import com.csc.dpms.da.util.ReplaceUtil;

/**
 * 產生所有文件
 * 
 * @author I20165
 *
 */
public class SpecCreator {
	private static Logger logger = LoggerFactory.getLogger(SpecCreator.class);
	private static final String SPEC_NAME = "SPEC.docx";
	private static GeneratorConfiguration _config;

	public static void create(GeneratorConfiguration config) {
		_config = config;
		// 載入動態編輯出來的 class
		Object userClass = invokeUserDefinedClass(config);

		// 程式規範
		XWPFDocument doc = new XWPFDocument();
		try {
			doc = new XWPFDocument(OPCPackage.open(Program.getTEMPLATE_PATH() + SPEC_NAME));
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// 中鋼程式規範第一層都用一個表格框起來, 所以不用考慮這個
//		for (XWPFParagraph p : doc.getParagraphs()) {
//			List<XWPFRun> runs = p.getRuns();
//			if (runs != null) {
//				for (XWPFRun r : runs) {
//					String text = r.getText(0);
//					if (text != null && text.contains("{program.id}")) {
//						text = text.replace("{program.id}", config.getProgram().getName());
//						r.setText(text, 0);
//					}
//				}
//			}
//		}

		for (int it = 0; it < doc.getTables().size(); it++) {
			XWPFTable tbl = doc.getTables().get(it);
			for (int ir = 0; ir < tbl.getRows().size(); ir++) {
				XWPFTableRow row = tbl.getRows().get(ir);
				for (int ic = 0; ic < row.getTableCells().size(); ic++) {
					XWPFTableCell cell = row.getTableCells().get(ic);
					// 分析字元
					//logger.debug("==== cell.getParagraphs().size(): {}", cell.getParagraphs().size());
					for (int ip = 0; ip < cell.getParagraphs().size(); ip++) {
						XWPFParagraph p = cell.getParagraphs().get(ip);
						p = replaceParagraphAtCell(p, config.getProgram(), cell);
						p = replaceParagraphAtCell(p, config.getTable(), cell);
						p = replaceParagraphAtCell(p, userClass, cell);
						
					}

					// 讀取內層 table(為了可以 table.addRow, 一定得用 for index 的寫法)
					for (int i = 0; i < cell.getTables().size(); i++) {
						XWPFTable innerTable = cell.getTables().get(i);
						for (int ii = 0; ii < innerTable.getRows().size(); ii++) {
							XWPFTableRow innerRow = innerTable.getRows().get(ii);
							for (XWPFTableCell innerCell : innerRow.getTableCells()) {
								for (XWPFParagraph innerP : innerCell.getParagraphs()) {
									innerP = replaceParagraphAtCell(innerP, config.getProgram(), innerCell);
									innerP = replaceParagraphAtCell(innerP, config.getTable(), innerCell);
									innerP = replaceParagraphAtTable(innerTable, innerRow, ii, innerP, userClass, innerCell);
								}
							}
						}
					}
				}
			}
		}
		try {
			doc.write(new FileOutputStream(String.format("%s.docx", config.getProgram().getId())));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	private static void cloneParagraphAtCell(XWPFParagraph source, XWPFTableCell cell) {
		//取得這個 paragraph 的位置
		XmlCursor cur = source.getCTP().newCursor();
		
		//再於次一位置中
		cur.toNextSibling();
		
		//在本 cell 插入一個新 paragraph
		XWPFParagraph newP = cell.insertNewParagraph(cur);
		
		//最後複製所有屬性, 文字
	    CTPPr pPr = newP.getCTP().isSetPPr() ? newP.getCTP().getPPr() : newP.getCTP().addNewPPr();
	    pPr.set(source.getCTP().getPPr());
	    for (XWPFRun r : source.getRuns()) {
	        XWPFRun nr = newP.createRun();
	        cloneRun(nr, r);
	    }
	}
	
	private static void cloneRun(XWPFRun clone, XWPFRun source) {
	    CTRPr rPr = clone.getCTR().isSetRPr() ? clone.getCTR().getRPr() : clone.getCTR().addNewRPr();
	    rPr.set(source.getCTR().getRPr());
	    clone.setText(source.getText(0));
	}


	/**
	 * 將內容裡的 ${class.field}置換為實際值
	 * 
	 * @param p
	 * @param clazz
	 * @param clazzInstance
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private static XWPFParagraph replaceParagraphAtCell(XWPFParagraph p, Object clazzInstance, XWPFTableCell cell) {
		Method[] allGetMethod = clazzInstance.getClass().getDeclaredMethods();
		for (Method method : allGetMethod) {
			if(!method.getName().startsWith("get")) {continue;}
			String symbol = String.format("${%s.%s}", clazzInstance.getClass().getSimpleName(),
					ParameterUtil.getterProperty(method.getName()));
			if(method.getReturnType().equals(String.class)) {
				String value;
				try {
					value = (String)method.invoke(clazzInstance);
					if (value==null) {continue;}
					String text = ReplaceUtil.replaceContentWithSymbol(p, clazzInstance.getClass().getSimpleName(),
							ParameterUtil.getterProperty(method.getName()), value);
					p.getRuns().get(0).setText(text, 0);
				} catch (Exception e) {
				}
			}else if(method.getReturnType().equals(List.class)) { //只會來自 UserClass
				List<String> listValue;
				try {
					//用強轉List<String>來測試可不可以往下跑...有沒有更好的方式?
					listValue = (List<String>)method.invoke(clazzInstance, _config, "dummy");
					if (listValue==null) {continue;}
					//logger.debug("======= symbol:{}", symbol);
					mergeRuns(p, symbol);
					if (p.getRuns().size()==0) {
						return p;
					}
					XWPFRun r = p.getRuns().get(0);
					String text = r.getText(0);
					if (text.contains(symbol)) {
						if (listValue != null) {
							//因為 cloneParagraphAtCell 是往下插入新段落,因此List也要倒著塞值
							for (int i = listValue.size()-1; i >=0 ; i--) {
								if(i==0){
									// 到達 List 最後元素才全部置換, 否則找出所有設定的地方 repeat 一行
									r.setText(text.replace(symbol, listValue.get(i)), 0);
								}else{
									r.setText(text.replace(symbol, listValue.get(i)), 0);
									cloneParagraphAtCell(p, cell);
								}
							}
						}
					}
				} catch (Exception e) {
					//logger.error("get {} fail!!", symbol, e);
					continue;
				}

			}
		}
		return p;
	}

	/**
	 * 將表格裡的 ${UserClass.getMethod}置換為多筆的 row
	 * 
	 * @param p
	 * @param clazz
	 * @param clazzInstance
	 * @return
	 */
	private static XWPFParagraph replaceParagraphAtTable(XWPFTable innerTable, XWPFTableRow innerRow, int ii, XWPFParagraph p, Object clazzInstance, XWPFTableCell cell) {
		Method[] allMethod = clazzInstance.getClass().getDeclaredMethods();
		Object paramsObj[] = { _config, "dummy" };
		for (Method method : allMethod) {
			try {
				@SuppressWarnings("unchecked")
				List<String> vlList = (List<String>) method.invoke(clazzInstance, paramsObj);
				String oldChar = String.format("${MyClass.%s}", method.getName());
				// 檢查被斷成好幾個 run 的 Paragraph, 是否含完整的 ${}, 若有就把所有的 run 併在第一個 run, 其他則清空
				// 因此只要比對第一個 run 即可
				mergeRuns(p, oldChar);
				if (p.getRuns().size()==0) {
					return p;
				}
				XWPFRun r = p.getRuns().get(0);
				String text = r.getText(0);
				if (text.contains(oldChar)) {
					if (vlList != null) {
						for (int jj = 1; jj < vlList.size() ; jj++) {
							r.setText(text.replace(oldChar,vlList.get(jj)), 0);
							innerTable.addRow(innerRow, ii+jj);
						}
						r.setText(text.replace(oldChar,vlList.get(0)), 0);
					}
				} else {
					continue;
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return p;
	}
	

	
	private static Object invokeUserDefinedClass(GeneratorConfiguration config) {
		// https://stackoverflow.com/questions/6219829/method-to-dynamically-load-java-class-files
		// Create a File object on the root of the directory containing the class file
		File file = new File(config.getUserDefineClass().getPath());
		Object myClass = null;

		try {
			// Convert File to a URL
			URL url = file.toURL(); // file:/c:/myclasses/
			URL[] urls = new URL[] { url };

			// Create a new class loader with the directory
			ClassLoader cl = new URLClassLoader(urls);

			// Load in the class; MyClass.class should be located in
			// the directory file:/c:/myclasses/com/mycompany
			Class<?> cls = cl.loadClass(config.getUserDefineClass().getClassName().replaceAll(".java", ""));
			myClass = cls.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return myClass;

	}

	//由於 POI會自己亂切字元,因此自己再合併回來
	private static void mergeRuns(XWPFParagraph p, String findText) {
		PositionInParagraph startPos = new PositionInParagraph();
		TextSegement textSeg = p.searchText(findText, startPos);
		if (textSeg != null) {
			String allText = "";
			for (int i = 0; i < p.getRuns().size(); i++) {
				allText = allText + p.getRuns().get(i).getText(0);
				p.getRuns().get(i).setText("", 0);
			}
			p.getRuns().get(0).setText(allText,0);
		}
	}

}
