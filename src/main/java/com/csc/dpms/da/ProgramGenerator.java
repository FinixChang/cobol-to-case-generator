package com.csc.dpms.da;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import javax.tools.JavaCompiler;
import javax.tools.StandardJavaFileManager;
import javax.tools.StandardLocation;
import javax.tools.ToolProvider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.dpms.da.source.vo.GeneratorConfiguration;

/**
 * codegen 主程式
 * 
 * @author I20165
 *
 */
public class ProgramGenerator {
	private static Logger logger = LoggerFactory.getLogger(ProgramGenerator.class);

	public static void main(String[] args) {
		try {
			assignRunSetting(args);

			// 讀取 batConfig.xml 轉成 vo
			GeneratorConfiguration config = ConfigParser.getBatchConfiguration(RunSetting.inFile);
			
			// 編譯自訂 Class, 以執行產生多筆程式碼邏輯
			compileUserDefineClass(config.getUserDefineClass().getPath(), config.getUserDefineClass().getClassName());

			// 產生程式規範
			SpecCreator.create(config);

			// 產生所有程式
			ObjectCreator.create(config);
		} catch (Exception e) {
			logger.error("發生不預期錯誤", e);
		}
	}

	private static void assignRunSetting(String[] configs) throws FileNotFoundException, IOException {
		for (String config : configs) {
			if (config.startsWith("inFile=")) {
				RunSetting.inFile = config.replace("inFile=", "");
			}
			if (config.startsWith("baseJavaDir=")) {
				RunSetting.baseJavaDir = config.replace("baseJavaDir=", "");
			}
			if (config.startsWith("baseWebInfDir=")) {
				RunSetting.baseWebInfDir = config.replace("baseWebInfDir=", "");
			}
			if (config.startsWith("baseResourceDir=")) {
				RunSetting.baseResourceDir = config.replace("baseResourceDir=", "");
			}
			if (config.startsWith("templateDir=")) {
				RunSetting.templateDir = config.replace("templateDir=", "");
			}
		}
	}

	private static void compileUserDefineClass(String listClassPath, String listClass) {
		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
		logger.info("**** compiler:{}", compiler);
		Charset utf8 = Charset.forName("UTF-8");
		StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, utf8);
		logger.info("**** fileManager:{}", fileManager);

		try {
			fileManager.setLocation(StandardLocation.CLASS_OUTPUT, Arrays.asList(new File(listClassPath)));
			// Compile the file
			File[] javaFiles = new File[] { new File(listClassPath, listClass) };
			compiler.getTask(null, null, null,
					null, null, fileManager.getJavaFileObjects(javaFiles)).call();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			fileManager.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
