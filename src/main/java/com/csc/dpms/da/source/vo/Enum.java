package com.csc.dpms.da.source.vo;

import javax.xml.bind.annotation.XmlAttribute;

public class Enum  extends BaseVo {
	private String value;
	private String name;
	private String property;

	public String getName() {
		return name;
	}

	@XmlAttribute
	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	@XmlAttribute
	public void setValue(String value) {
		this.value = value;
	}

	public String getProperty() {
		return property;
	}

	@XmlAttribute
	public void setProperty(String property) {
		this.property = property;
	}

}
