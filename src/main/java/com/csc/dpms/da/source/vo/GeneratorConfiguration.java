package com.csc.dpms.da.source.vo;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.csc.dpms.da.RunSetting;

//有 array 的寫法
@XmlRootElement(name="generatorConfiguration")
@XmlAccessorType(XmlAccessType.FIELD)
public class GeneratorConfiguration  extends BaseVo{
	@XmlElement
	private ProduceProperty produceProperty;

	@XmlElement
	private RunSetting runSetting;

	@XmlElement
	private Table table;
	
	@XmlElement
	private Program program;
	
	@XmlElement
	private UserDefineClass userDefineClass;
	
	public Program getProgram() {
		return program;
	}

	public void setProgram(Program program) {
		this.program = program;
	}

	public RunSetting getRunSetting() {
		return runSetting;
	}

	public void setRunSetting(RunSetting runSetting) {
		this.runSetting = runSetting;
	}

	public Table getTable() {
		return table;
	}
	
	public void setTable(Table table) {
		this.table = table;
	}

	public ProduceProperty getProduceProperty() {
		return produceProperty;
	}
	
	public void setProduceProperty(ProduceProperty produceProperty) {
		this.produceProperty = produceProperty;
	}

	public UserDefineClass getUserDefineClass() {
		return userDefineClass;
	}

	public void setUserDefineClass(UserDefineClass userDefineClass) {
		this.userDefineClass = userDefineClass;
	}
	

}
