package com.csc.dpms.da.source.vo;

import javax.xml.bind.annotation.XmlAttribute;

public class ProduceProperty extends BaseVo {
	private Boolean overwriteProgram = false;
	public Boolean getOverwriteProgram() {
		return overwriteProgram;
	}

	@XmlAttribute
	public void setOverwriteProgram(Boolean overwriteProgram) {
		this.overwriteProgram = overwriteProgram;
	}
	
}
