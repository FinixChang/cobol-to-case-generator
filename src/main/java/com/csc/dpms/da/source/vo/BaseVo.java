package com.csc.dpms.da.source.vo;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

import com.csc.dpms.da.util.ParameterUtil;

public class BaseVo implements IBaseVo {
	/**
	 * 使用 reflection把所有屬性中的基本型別都 logger 出來
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		String result = String.format("[Class %s]", this.getClass().getSimpleName());
		
		//找出 getter 且回傳 String所有 method
		Method[] allGetMethod = Arrays.stream(this.getClass().getDeclaredMethods())
				.filter(x -> x.getName().startsWith("get") && x.getReturnType().equals(String.class))
				.toArray(Method[]::new); 
		for (Method method : allGetMethod) {
			String value;
			try {
				value = (String)method.invoke(this);
				if (value==null) {continue;}
				result = String.format("%s ${%s.%s} = %s|\n", result, this.getClass().getSimpleName(),
						ParameterUtil.getterProperty(method.getName()), value);
			} catch (Exception e) {
			}
		}
		
		//找出 getter 且回傳 List,直接呼叫 toString
		allGetMethod = Arrays.stream(this.getClass().getDeclaredMethods())
				.filter(x -> x.getName().startsWith("get") && x.getReturnType().equals(List.class))
				.toArray(Method[]::new); 
		for (Method method : allGetMethod) {
			List<Object> listValue;
			try {
				listValue = (List<Object>)method.invoke(this);
				if (listValue==null) {continue;}
				result = String.format("%s ${%s.%s} = %s|\n", result, this.getClass().getSimpleName(),
						ParameterUtil.getterProperty(method.getName()), listValue.toString());
			} catch (Exception e) {
				//e.printStackTrace();
				continue;
			}
		}
		
		return result;
	}
}
