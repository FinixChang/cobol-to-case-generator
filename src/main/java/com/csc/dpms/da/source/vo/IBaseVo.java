package com.csc.dpms.da.source.vo;

public interface IBaseVo {

	/**
	 * 使用 reflection把所有屬性中的基本型別都 logger 出來
	 */
	@Override
	public String toString();
}
