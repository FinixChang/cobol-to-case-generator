package com.csc.dpms.da.source.vo;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import com.csc.dpms.da.RunSetting;

public class Program extends BaseVo {
	private static String TEMPLATE_PATH;
	public static String getTEMPLATE_PATH() {
		return String.format("%s\\%s\\%s\\", RunSetting.templateDir, getType(), getTemplate()) ;
	}

	private String id;
	private String name;
	private String author;

	public static List<Create> creates;

	
	
	public String getAuthor() {
		return author;
	}

	@XmlAttribute
	public void setAuthor(String author) {
		this.author = author;
	}

	private static String system;
	private static String type;
	private static String operation;
	private String appId;
	private static String template;
	
	
	public String getTransId() {
		return id.substring(0, system.length()+2).toLowerCase(); //交易代號 = 系統代碼 + 2碼 - 小寫
	}

	public String getRule() {
		return id.replace("Controller", "Rule"); //Rule = Controller 換成 Rule
	}


	public static String getType() {
		return type;
	}

	@XmlAttribute
	public void setType(String type) {
		this.type = type;
	}

	public static String getOperation() {
		return operation;
	}

	@XmlAttribute
	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getAppId() {
		return ("".equals(appId)||appId==null) ? getTransId() : appId; 
	}

	public String getAnnotationAppId() {
		return ("".equals(appId)||appId==null) ? "" : String.format("(appId = \"%s\")", appId.toUpperCase());
	}

	@XmlAttribute
	public void setAppId(String appId) {
		this.appId = appId;
	}

	public static String getTemplate() {
		return template;
	}

	@XmlAttribute
	public void setTemplate(String template) {
		this.template = template;
	}

	public String getName() {
		return name;
	}

	@XmlAttribute
	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	@XmlAttribute
	public void setId(String id) {
		this.id = id;
	}

	public static String getSystem() {
		return system;
	}

	@XmlAttribute
	public void setSystem(String system) {
		this.system = system;
	}

	public String getUrl() {
		// '/dqw/basic/dqwbp'
		return String.format("/%s/%s/%s", getSystem(), getOperation(), getTransId());
	}

	public List<Create> getCreates() {
		return creates;
	}

	@XmlElement(name = "create")
	public void setCreates(List<Create> creates) {
		this.creates = creates;
	}

}
