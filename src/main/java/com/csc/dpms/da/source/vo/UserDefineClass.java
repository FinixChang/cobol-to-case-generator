package com.csc.dpms.da.source.vo;

import javax.xml.bind.annotation.XmlAttribute;


public class UserDefineClass extends BaseVo {
	private String path;
	private String className;

	public String getPath() {
		return path;
	}

	@XmlAttribute
	public void setPath(String path) {
		this.path = path;
	}

	public String getClassName() {
		return className;
	}

	@XmlAttribute
	public void setClassName(String className) {
		this.className = className;
	}

}
