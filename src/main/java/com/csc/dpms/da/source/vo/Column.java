package com.csc.dpms.da.source.vo;


import java.lang.reflect.Method;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import com.csc.dpms.da.util.ParameterUtil;


@XmlAccessorType(XmlAccessType.FIELD)
public class Column extends BaseVo {
	@XmlAttribute
	private String id;

	@XmlAttribute
	private String Name;

	@XmlAttribute
	private String property;

	@XmlAttribute
	private String type;

	@XmlAttribute
	private String inputType;

	@XmlAttribute
	private String primary;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}


	@XmlElement(name = "enum")
	private List<Enum> enums;

	public List<Enum> getEnums() {
		return enums;
	}

	public void setEnums(List<Enum> enums) {
		this.enums = enums;
	}

	
	public String getProperty() {
		return property == null ? idToProperty() : property;
	}
	
	/**
	 * 將 id 轉為  property : 僅簡化為「去底線, 去尾」
	 */
	private String idToProperty(){
		String[] tokens = id.toLowerCase().split("_");
		if (tokens.length == 0){ //沒有底線
			return id.toLowerCase();
		}
		String result = tokens[0];
		for (int i = 1; i < tokens.length-1; i++) { //去尾
			//result =  result + token第一碼轉大寫 + token其他碼
			result = String.format("%s%s%s", result, tokens[i].substring(0, 1).toUpperCase() , tokens[i].substring(1));
		}	
		return result;
	}

	public void setProperty(String property) {
		this.property = property;
	}

	public String getType() {
		return type == null ? "string" : type ;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getInputType() {
		return inputType == null ? "text" : inputType ;
	}

	public void setInputType(String inputType) {
		this.inputType = inputType;
	}

	public String getPrimary() {
		//未設為 true, 一律視為 false
		return primary == null ? "false" : ("true".equals(primary) ? "true" : "false") ;
	}

	public void setPrimary(String primary) {
		this.primary = primary;
	}

	public String getEnumClassName() {
		if (getEnums().isEmpty()){
			return "";
		}
		return ParameterUtil.toCascade(Program.getSystem().toUpperCase(), getProperty() + "Name");
	}

	public String getEnumVarAndClassName() {
		if (getEnums().isEmpty()){
			return "";
		}
		return String.format("\"%ss\", %s", getProperty() ,getEnumClassName());
	}

	//自訂 public method
	public static String getValueBy(Table table, Method method, String id){
		String value = "";
		//找出傳進來的 id 跟  Column.id 一樣的  Column, 並用 method取得其值(例如 .getProperty())
		List<Column> primaryList = table.getColumnList().stream()
			    .filter(p -> id.equals(p.getId())).collect(Collectors.toList());
		try {
			value = (String) method.invoke(primaryList.get(0));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return value;
	} 



}
