package com.csc.dpms.da.source.vo;

import javax.xml.bind.annotation.XmlAttribute;

public class Create extends BaseVo {
	private String programType;

	private String objectType;

	private String templateFile;

	private String outputPath;

	private String outputFile;
	
	private String speciallReplace;

	private String mustExist;

	private String overwrite;

	public String getProgramType() {
		return programType;
	}

	@XmlAttribute
	public void setProgramType(String programType) {
		this.programType = programType;
	}

	public String getObjectType() {
		return objectType;
	}

	@XmlAttribute
	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public String getTemplateFile() {
		return templateFile;
	}

	@XmlAttribute
	public void setTemplateFile(String templateFile) {
		this.templateFile = templateFile;
	}

	public String getOutputPath() {
		return outputPath;
	}

	@XmlAttribute
	public void setOutputPath(String outputPath) {
		this.outputPath = outputPath;
	}

	public String getOutputFile() {
		return outputFile;
	}

	@XmlAttribute
	public void setOutputFile(String outputFile) {
		this.outputFile = outputFile;
	}

	public String getSpeciallReplace() {
		return speciallReplace;
	}

	@XmlAttribute
	public void setSpeciallReplace(String speciallReplace) {
		this.speciallReplace = speciallReplace;
	}

	public String getMustExist() {
		return mustExist;
	}

	@XmlAttribute
	public void setMustExist(String mustExist) {
		this.mustExist = mustExist;
	}

	public String getOverwrite() {
		return overwrite;
	}

	@XmlAttribute
	public void setOverwrite(String overwrite) {
		this.overwrite = overwrite;
	}

}