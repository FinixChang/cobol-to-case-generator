package com.csc.dpms.da.source.vo;


import java.lang.reflect.Method;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.beust.jcommander.internal.Lists;
import com.csc.dpms.da.util.ParameterUtil;

//有 array 的寫法
@XmlRootElement(name="table")
@XmlAccessorType(XmlAccessType.FIELD)
public class Table extends BaseVo {
	private static String id;

	@XmlAttribute
	private String search;
	
	@XmlAttribute
	private String desc;
	
	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}




	@XmlAttribute
	private String columns;

	private static List<Column> columnList;
	

	public static String getId() {
		return id;
	}

	@XmlAttribute
	public void setId(String id) {
		this.id = id;
	}
	public String getColumns() {
		return columns;
	}

	public void setColumns(String columns) {
		this.columns = columns;
	}

	public List<Column> getColumnList() {
		return columnList;
	}

	@XmlElement(name = "column")
	public void setColumnList(List<Column> columnList) {
		this.columnList = columnList;
	}
	
	

	
	public String getDataSource() {
		//id=udb.dbcsca.DB.TBDQBP ==> dataSource = udb.dbcsca 
		String[] tokens = id.replace(".", "_").split("_");
		return String.format("%s.%s", tokens[0], tokens[1]);
	}
	
	public String getDbServer() {
		String[] tokens = id.replace(".", "_").split("_");
		return tokens[0];
	}
	
	public String getDatabase() {
		String[] tokens = id.replace(".", "_").split("_");
		return tokens[1];
	}
	
	public String getMapper() {
		//id=udb.dbcsca.DB.TBDQBP ==> dataSource = DQW.TBDQBP 
		String[] tokens = id.replace(".", "_").split("_");
		return String.format("%s.%s", Program.getSystem().toUpperCase(), tokens[3]);
	}

	public String getName() {
		//id=udb.dbcsca.DB.TBDQBP ==> name=TBDQBP
		String[] tokens = id.replace(".", "_").split("_");
		return tokens[tokens.length-1];
	}

	public String getVo() {
		//id=udb.dbcsca.DB.TBDQBP ==> vo=DQBPVo
		return String.format("%sVo", getName().replace("TB", ""));
	}

	public String getVoVar() {
		//id=udb.dbcsca.DB.TBDQBP ==> voVar=dqbpVo
		return String.format("%sVo", getName().replace("TB", "").toLowerCase());
	}

	public String getPrimaryId() {
		return getPrimary().size() == 1 ? getPrimary().get(0).getId() : "";
	}

	public String getPrimaryProperty() {
		return getPrimary().size() == 1 ? getPrimary().get(0).getProperty() : "";
	}
	
	public String getPrimaryName() {
		return getPrimary().size() == 1 ? getPrimary().get(0).getName() : "";
	}

	public String getPrimaryGetter() {
		return getPrimary().size() == 1 ? ParameterUtil.toCascade("get", getPrimary().get(0).getProperty()+"()") : "";
	}

	public String getPrimarySetter() {
		return getPrimary().size() == 1 ? ParameterUtil.toCascade("set", getPrimary().get(0).getProperty()+"(\"\")") : "";
	}
	
	private List<Column> getPrimary(){
		//找出 columnList 裡的 primary=true
		List<Column> primaryList = columnList.stream()
			    .filter(p -> "true".equals(p.getPrimary())).collect(Collectors.toList());
		return primaryList;
	}

	public List<Column> getEnumColumn(){
		//找出 columnList 裡的 Display.size > 0
//		List<Column> enumColumnList = getColumnList().stream()
//			    .filter(p -> p.getEnums().size()>0).collect(Collectors.toList());
		List<Column> enumColumnList = Lists.newArrayList();
		for (Column column : getColumnList()) {
			if(column.getEnums()!=null) {
				enumColumnList.add(column);
			}
		}
		return enumColumnList;
	}


	public String getPlaceHolder() {
		String[] searchArray = getSearch().split(",");
		List<String> result = Lists.newArrayList();
		Method getName = null;
		try {
			getName = Column.class.getMethod("getName");
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//拿設定在 search 裡的欄位定義, 去轉換出其 property
		for (String columnId : searchArray) {
			result.add(Column.getValueBy(this, getName, columnId));
		}
		return String.join("、",  result);
	}


	public String getSchema() {
		//id=udb.dbcsca.DB.TBDQBP ==> dataSource = DQW.TBDQBP 
		String[] tokens = id.replace(".", "_").split("_");
		return tokens[2];
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}
