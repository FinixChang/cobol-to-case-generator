package com.csc.dpms.da;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.dpms.da.source.vo.GeneratorConfiguration;

/**
 * 載入 config 並轉換為 Vo
 * @author I20165
 *
 */
public class ConfigParser {
	private static Logger logger = LoggerFactory.getLogger(ConfigParser.class);
	public static void main(String[] args){
		getBatchConfiguration("programConfig.xml");
	}
	public static GeneratorConfiguration getBatchConfiguration(String filePath) {
		GeneratorConfiguration generatorConfiguration = new GeneratorConfiguration();
		try {

			File file = new File(filePath);
			JAXBContext jaxbContext = JAXBContext.newInstance(GeneratorConfiguration.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			generatorConfiguration = (GeneratorConfiguration) jaxbUnmarshaller.unmarshal(file);
			logger.info("{}" , generatorConfiguration);
			logger.info("{}" , generatorConfiguration.getProduceProperty());
			logger.info("{}" , generatorConfiguration.getRunSetting());
			logger.info("{}" , generatorConfiguration.getProgram());
			logger.info("{}" , generatorConfiguration.getTable());
			logger.info("{}" , generatorConfiguration.getUserDefineClass());
		  } catch (JAXBException e) {
			e.printStackTrace();
		  }
		return generatorConfiguration;
	}

}
