package com.csc.dpms.da;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.collections.Lists;

import com.csc.dpms.da.source.vo.Create;
import com.csc.dpms.da.source.vo.GeneratedSource;
import com.csc.dpms.da.source.vo.GeneratorConfiguration;
import com.csc.dpms.da.source.vo.Program;
import com.csc.dpms.da.util.FileUtil;
import com.csc.dpms.da.util.ParameterUtil;
import com.csc.dpms.da.util.ReplaceUtil;

/**
 * 產生所有文件
 * 
 * @author I20165
 *
 */
public class ObjectCreator {
	private static Logger logger = LoggerFactory.getLogger(ObjectCreator.class);
	private static GeneratorConfiguration _config;
	private static List<GeneratedSource> allFile = Lists.newArrayList();

	public static void create(GeneratorConfiguration config) {
		if (!FileUtil.isFolderExist(Program.getTEMPLATE_PATH())) {
			logger.error("== 樣版類別路徑 {} 尚未建立，請通知模版{}提供者者!! ==", Program.getTEMPLATE_PATH(), Program.getTemplate());
			return;
		}

		_config = config;
		String sourceTemplate = "";
		String fileName = "";
		String path = "";
		StringBuilder sb = new StringBuilder();
		// 載入動態編輯出來的 class
		Object _userClass = invokeUserDefinedClass(config);

		for (Create createFile : config.getProgram().getCreates()) {
			// 讀入樣版檔
			try {
				sourceTemplate = FileUtil.readStringFromFile(Program.getTEMPLATE_PATH() + createFile.getTemplateFile())
						.toString();
			} catch (IOException e) {
				logger.error("=== {} 範本不存在!! ===", createFile.getProgramType(), e);
				continue;
			}

			fileName = getBindingValueOf(createFile.getOutputFile(), config);
			path = getBindingValueOf(createFile.getOutputPath(), config);

			sourceTemplate = addContent(sourceTemplate, createFile, path, fileName);
			if ("".equals(sourceTemplate)) {
				continue;
			}

			sb = createSourceCode(config, sourceTemplate, fileName, _userClass);
			sb = specialReplace(sb, createFile.getSpeciallReplace());

			createSourceCode(allFile, fileName, path, sb, createFile);
		}

	}

	/**
	 * 對現有檔案如 ExtendVo, SqlMapper 加工
	 * 
	 * @param sourceTemplate
	 * @param createFile
	 * @param path
	 * @param fileName
	 * @return
	 */
	private static String addContent(String sourceTemplate, Create createFile, String path, String fileName) {
		// ExtendVo, SqlMapper 需讀得到檔案再插入樣版檔
		if ("true".equals(createFile.getMustExist())) {
			String oldContent = "";
			try {
				oldContent = FileUtil.readStringFromFile(path + fileName + "." + createFile.getObjectType());
			} catch (Exception e) {
				logger.error(" {}{} 檔案不存在!", path, fileName);
				return "";
			}

			// 找出檔案裡插入的樣版檔範圍
			String prefix = "";
			String suffix = "";
			String lastKeyword = "";
			switch (createFile.getObjectType()) {
				case "java":
					prefix = "//--";
					lastKeyword = "}";
					break;

				case "xml":
					prefix = "<!--";
					suffix = "-->";
					lastKeyword = "<";
					break;

				default:
					break;
			}
			String replaceBeginStr = String.format("%s [case-program-generator:%s] (begin) %s", prefix,
					Program.getTemplate(), suffix);
			String replaceEndStr = String.format("%s [case-program-generator:%s] (end) %s", prefix,
					Program.getTemplate(), suffix);
			int replaceBegin = oldContent.indexOf(replaceBeginStr) - 1;
			int replaceEnd = oldContent.indexOf(replaceEndStr) + replaceEndStr.length() + 1;
			if (replaceBegin < 0) {
				replaceBegin = oldContent.lastIndexOf(lastKeyword) - 1; // 找出最後關鍵字
				replaceEnd = replaceBegin + 1;
			} else {
				// 在已經加過的情況下, 可以設為不再覆蓋(通常是 SqlMapper,因為會有自己撰寫的語法)
				if ("false".equals(createFile.getOverwrite())) {
					return "";
				}
			}
			oldContent = String.format("%s\n%s\n%s\n%s\n%s", oldContent.substring(0, replaceBegin), replaceBeginStr,
					sourceTemplate, replaceEndStr, oldContent.substring(replaceEnd));
			sourceTemplate = oldContent;

		}
		return sourceTemplate;
	}

	/**
	 * 把 programConfig.xml 中的 symbol(例如:Runsetting.property、Program.property)換成實際值
	 * 
	 * @param str
	 * @return
	 */
	private static String getBindingValueOf(String str, GeneratorConfiguration config) {
		if (!str.contains("$")) {
			return str;
		}
		String newStr = replaceClassProperties(str, config.getProgram(), "dummy");
		newStr = replaceClassProperties(newStr, config.getTable(), "dummy");
		newStr = replaceClassProperties(newStr, config.getRunSetting(), "dummy");
		logger.debug("** getBindingValueOf original string {} value is {}", str, newStr);
		return newStr;
	}

	private static void createSourceCode(List<GeneratedSource> allFile, String fileName, String path, StringBuilder sb,
			Create createFile) {
		try {
			fileName = String.format("%s.%s", fileName, createFile.getObjectType());
			FileUtil.createFile(path, fileName, sb);
		} catch (IOException e) {
			logger.error("== {} 無法建立!! ==", createFile.getProgramType(), e);
		}
		logger.info("====== {} {} Make =======", createFile.getProgramType(), fileName);
		GeneratedSource generatedSource = new GeneratedSource();
		generatedSource.setPath(path);
		generatedSource.setFileName(fileName);
		allFile.add(generatedSource);
	}

	/**
	 * 找出 oldChar 那行,copy 後把 oldChar 換成 newChar
	 * 
	 * @param sourceTemplate
	 * @param oldChar
	 * @param newChar
	 * @return
	 */
	private static String repeatAndSetTemplate(String sourceTemplate, String oldChar, String newChar) {
		String[] lines = sourceTemplate.split("\n");
		List<String> lineList = Arrays.asList(lines);
		List<String> newList = Lists.newArrayList();
		for (String string : lineList) {
			if (string.indexOf(oldChar) >= 0) {
				newList.add(string.replace(oldChar, newChar));
			}
			newList.add(string);
		}
		return String.join("\n", newList);
	}

	/**
	 * 轉換特定關鍵字 格式為 v1->r1:v2->r2:v3->r3....
	 * 
	 * @param sb
	 * @param speciallReplace
	 * @return
	 */
	private static StringBuilder specialReplace(StringBuilder sb, String speciallReplace) {
		if (StringUtils.isBlank(speciallReplace)) {
			return sb;
		}
		StringBuilder result = new StringBuilder();
		String[] replaceArray = speciallReplace.split(":");
		String text = sb.toString();
		for (String string : replaceArray) {
			String[] replace = string.split("->");
			text = text.replace(replace[0], replace[1]);
		}
		result.append(text);
		return result;
	}

	/**
	 * 產製 Controller
	 * 
	 * @param config
	 */
	private static StringBuilder createSourceCode(GeneratorConfiguration config, String sourceTemplate,
			String className, Object userClass) {
		StringBuilder result = new StringBuilder();

		sourceTemplate = replaceClassProperties(sourceTemplate, config.getProgram(), className);
		sourceTemplate = replaceClassProperties(sourceTemplate, config.getTable(), className);
		sourceTemplate = replaceClassProperties(sourceTemplate, userClass, className);
		sourceTemplate = sourceTemplate.replace("${this.Class}", className);

		// logger.info(" ## Controller ## : {}", controller);

		result.append(sourceTemplate);
		return result;

	}

	/**
	 * 將內容裡的 ${class.field}置換為實際值
	 * 
	 * @param content
	 * @param clazz
	 * @param clazzInstance
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private static String replaceClassProperties(String content, Object clazzInstance, String fileName) {
		// 找出 getter(lambda無法處理動態compile出來的 class ?)
		// Method[] allGetMethod =
		// Arrays.stream(clazzInstance.getClass().getDeclaredMethods())
		// .filter(x -> x.getName().startsWith("get") &&
		// x.getReturnType().equals(String.class))
		// .toArray(Method[]::new);
		Method[] allGetMethod = clazzInstance.getClass().getDeclaredMethods();
		for (Method method : allGetMethod) {
			if (!method.getName().startsWith("get")) {
				continue;
			}
			String symbol = String.format("${%s.%s}", clazzInstance.getClass().getSimpleName(),
					ParameterUtil.getterProperty(method.getName()));
			if (method.getReturnType().equals(String.class)) {
				String value;
				try {
					value = (String) method.invoke(clazzInstance);
					if (value == null) {
						continue;
					}
					content = ReplaceUtil.replaceContentWithSymbol(content, clazzInstance.getClass().getSimpleName(),
							ParameterUtil.getterProperty(method.getName()), value);

				} catch (Exception e) {
					logger.error("", e);
				}
			} else if (method.getReturnType().equals(List.class)) { // 只會來自 UserClass
				List<String> listValue;
				try {
					// 用強轉List<String>來測試可不可以往下跑...有沒有更好的方式?
					listValue = (List<String>) method.invoke(clazzInstance, _config, fileName);
					if (listValue == null) {
						continue;
					}
					// logger.debug("======= symbol:{}", symbol);
					for (String string : listValue) {
						if (string.equals(listValue.get(listValue.size() - 1))) {
							content = content.replace(symbol, string);
						} else {
							content = repeatAndSetTemplate(content, symbol, string);
						}
					}
				} catch (Exception e) {
					// logger.error("get {} fail!!", symbol, e);
					continue;
				}

			}
		}
		return content;
	}

	@SuppressWarnings({ "deprecation", "resource" })
	private static Object invokeUserDefinedClass(GeneratorConfiguration config) {
		// https://stackoverflow.com/questions/6219829/method-to-dynamically-load-java-class-files
		// Create a File object on the root of the directory containing the class file
		File file = new File(config.getUserDefineClass().getPath());
		Object myClass = null;

		try {
			// Convert File to a URL
			URL url = file.toURL(); // file:/c:/myclasses/
			URL[] urls = new URL[] { url };

			// Create a new class loader with the directory
			ClassLoader cl = new URLClassLoader(urls);

			// Load in the class; MyClass.class should be located in
			// the directory file:/c:/myclasses/com/mycompany
			Class<?> cls = cl.loadClass(config.getUserDefineClass().getClassName().replaceAll(".java", ""));
			myClass = cls.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return myClass;

	}

}
