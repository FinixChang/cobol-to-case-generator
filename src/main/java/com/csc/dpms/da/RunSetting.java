package com.csc.dpms.da;

import javax.xml.bind.annotation.XmlAttribute;

import com.csc.dpms.da.source.vo.BaseVo;

/**
 * 啟動設定 - 可外部修改參數
 * @author I20165
 *
 */
public class RunSetting  extends BaseVo {
	public static String inFile = "programConfig.xml";

	@XmlAttribute
	public static String baseJavaDir = "src\\main\\java\\com\\csc";

	@XmlAttribute
	public static String baseResourceDir = "src\\main\\resources\\com\\csc";

	@XmlAttribute
	public static String baseWebInfDir = "webapp\\WEB-INF\\views";

	public static String getBaseJavaDir() {
		return baseJavaDir;
	}

	public static void setBaseJavaDir(String baseJavaDir) {
		RunSetting.baseJavaDir = baseJavaDir;
	}

	public static String getBaseResourceDir() {
		return baseResourceDir;
	}

	public static void setBaseResourceDir(String baseResourceDir) {
		RunSetting.baseResourceDir = baseResourceDir;
	}

	public static String getBaseWebInfDir() {
		return baseWebInfDir;
	}

	public static void setBaseWebInfDir(String baseWebInfDir) {
		RunSetting.baseWebInfDir = baseWebInfDir;
	}

	public static String getTemplateDir() {
		return templateDir;
	}

	public static void setTemplateDir(String templateDir) {
		RunSetting.templateDir = templateDir;
	}

	@XmlAttribute
	public static String templateDir = "codeGenTemplate"; 
}
