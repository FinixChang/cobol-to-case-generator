package com.csc.dpms.da.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.xwpf.usermodel.PositionInParagraph;
import org.apache.poi.xwpf.usermodel.TextSegement;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 將內容含 sysmbol加上 string method運算者, replace 成也經過同樣運算的值
 * 
 * @author PEARL
 *
 */
public class ReplaceUtil {
	private static Logger logger = LoggerFactory.getLogger(ReplaceUtil.class);

	public static String replaceContentWithSymbol(XWPFParagraph p, String className, String method, String value) {
		String symbol = String.format("${%s.%s}", className, method);
		String sysmbolWithStringMethod = String.format("\\$\\{%s.%s+((\\.[\\w-]+)\\(+([\\w-\\,\\s]+)*\\))+\\}",
				className, method);
		mergeRuns(p, symbol);
		if (p.getRuns().size()==0) {
			return p.getRuns().get(0).getText(0);
		}
		XWPFRun r = p.getRuns().get(0);
		String content = r.getText(0);
		//if (!content.contains(symbol)) {return content;}

		// String 加入轉大小寫功能
		content = content.replace(symbol, value);
		Pattern pattern = Pattern.compile(sysmbolWithStringMethod);
		Matcher matcher = pattern.matcher(p.getText());
		while (matcher.find()) {
			logger.debug("==== sysmbolWithStringMethod(spec) : {}, group:{}", sysmbolWithStringMethod, matcher.group());
			mergeRuns(p, matcher.group());
			content = replaceWithStringMethod(p.getText(), matcher.group(), value);
			matcher = pattern.matcher(content);
		}
		return content;
	}

	//由於 POI會自己亂切字元,因此自己再合併回來
	private static void mergeRuns(XWPFParagraph p, String findText) {
		PositionInParagraph startPos = new PositionInParagraph();
		TextSegement textSeg = p.searchText(findText, startPos);
		if (textSeg != null) {
			String allText = "";
			for (int i = 0; i < p.getRuns().size(); i++) {
				allText = allText + p.getRuns().get(i).getText(0);
				p.getRuns().get(i).setText("", 0);
			}
			p.getRuns().get(0).setText(allText,0);
		}
	}

	public static String replaceContentWithSymbol(String content, String className, String method, String value) {
		String symbol = String.format("${%s.%s}", className, method);
		String sysmbolWithStringMethod = String.format("\\$\\{%s.%s+((\\.[\\w-]+)\\(+([\\w-\\,\\s]+)*\\))+\\}",
				className, method);
		// String 加入轉大小寫功能
		content = content.replace(symbol, value);
		Pattern pattern = Pattern.compile(sysmbolWithStringMethod);
		Matcher matcher = pattern.matcher(content);
		while (matcher.find()) {
//			logger.debug("==== sysmbolWithStringMethod : {}, group:{}", sysmbolWithStringMethod, matcher.group());
			content = replaceWithStringMethod(content, matcher.group(), value);
			matcher = pattern.matcher(content);
		}
		return content;
	}
	private static String replaceWithStringMethod(String original, String methods, String newValue) {
		String result = original;
		String[] methodArray = methods.substring(2, methods.length() - 1).replace(".", "-").split("-");
		for (String method : methodArray) {
			try {
				if (!method.contains("(")) {
					continue;
				}
				String methodName = method.substring(0, method.indexOf("("));

				Method[] allGetMethod = String.class.getDeclaredMethods();
				for (Method stringMethod : allGetMethod) {
					if (!stringMethod.getName().equals(methodName)) {
						continue;
					}
					Class<?>[] params = stringMethod.getParameterTypes();
					String parameter = method.substring(method.indexOf("(") + 1, method.indexOf(")"));
					String[] parameterArray = "".equals(parameter) ? new String[] {} : parameter.split(",");
					if (params.length != parameterArray.length) {
						continue;
					}
					Object[] array = {};
					if (params.length > 0) {
						array = new Object[params.length];
					}
					for (int i = 0; i < array.length; i++) {
						if (params[i].getName().equals("int")) {
							array[i] = Integer.parseInt(parameterArray[i]);
						}
					}
					try {
						newValue = (String) stringMethod.invoke(newValue, array);
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						logger.error(" *** exec string method error:{}", method);
						continue;
					}
				}

			} catch (SecurityException e) {
				logger.error(" *** string method error:{}", method);
				continue;
			}
		}
		result = result.replace(methods, newValue);
		return result;
	}
}
