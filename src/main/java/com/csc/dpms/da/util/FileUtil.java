package com.csc.dpms.da.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.dpms.da.source.vo.GeneratedSource;

/**
 * 檔案存取 utility
 * @author PEARL
 *
 */
public class FileUtil {
	private static Logger logger = LoggerFactory.getLogger(FileUtil.class);

	/**
	 * 從 resources 讀樣本檔
	 * @param fileName
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static StringBuilder getTemplate(String fileName) throws FileNotFoundException, IOException {
		StringBuilder result = new StringBuilder("");

		//Get file from resources folder
		InputStream in = FileUtil.class.getResourceAsStream(fileName); 
		BufferedReader reader = new BufferedReader(new InputStreamReader(in, "utf-8"));
		String line;
		try {

			while ((line = reader.readLine()) != null) {
				result.append(line + "\r\n");
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
			
		return result;
	}
	
	public static File createFile(String path, String outFilename, String content) throws IOException {
		createFolder(path);
		return createFile(path + outFilename, content);
	}

	
	public static File createFile(String outFilename, String content) throws IOException {
		StringBuilder sb = new StringBuilder();
		sb.append(content);
		return createFile(outFilename, sb);
	}

	public static File createFile(String path, String outFilename, StringBuilder content) throws IOException {
		//java 無法以程式存取 src/main/java, 只能直接操作 window 指令
		String[] args = ("CMD:/C:mkdir:" + path).split(":");
		Process p = Runtime.getRuntime().exec(args);
		try {
			p.waitFor();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return createFile(path + outFilename, content);
	}

	public static File createFile(String outFilename, StringBuilder content) throws IOException {
		File fileDir = new File(outFilename);
		Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileDir), "utf-8"));
		out.append(content.toString().substring(0, content.toString().length()));
		out.flush();
		out.close();
		return fileDir;
	}


	public static String readStringFromFile(String fileName) throws IOException {
	    BufferedReader br = new BufferedReader( new InputStreamReader(
                new FileInputStream(fileName), "UTF8"));
	    try {
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();

	        while (line != null) {
	            sb.append(line);
	            sb.append("\n");
	            line = br.readLine();
	        }
	        return sb.toString();
	    } finally {
	        br.close();
	    }
	}

	public static String readFile(GeneratedSource gs) throws IOException {
	    return readStringFromFile(gs.getPath() + gs.getFileName());
	}
	
	
	public static void createFolder(String dir) throws IOException {
		File fileDir = new File(dir);
		if (!fileDir.exists()) {
		    try{
		        fileDir.mkdir();
		    } 
		    catch(SecurityException se){
		        //handle it
		    	logger.error(" ***** create folder {} fail", dir, se);
		    }        
		}		
		return;
	}
	
	/**
	 * 判斷資料夾存不存在
	 * @param dir
	 * @return
	 * @throws IOException
	 */
	public static boolean isFolderExist(String dir) {
		File fileDir = new File(dir);
		return fileDir.exists();
	}
	

}
