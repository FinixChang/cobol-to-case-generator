package com.csc.dpms.da.util;

public class ParameterUtil {
	//因為參數內容是 json, 而參數又是 bpms 的參數, 因此需要進行 html encode
	public static String getEncodeParameter(String parameter) {
		//雙引號轉 &quot;
		parameter = parameter.replace("\'", "&quot;"); 
		return parameter;
	}
	
	/**
	 * 將文字轉 pascal規範 : head + tail第一碼轉大寫
	 * @param text
	 * @return
	 */
	public static String toCascade(String head, String tail) {
		return String.format("%s%s%s", head, tail.substring(0,1).toUpperCase(), tail.substring(1));
	}

	public static String getterProperty(String getter) {
		String result = getter.replace("get", "");
		return String.format("%s%s", result.substring(0,1).toLowerCase(), result.substring(1));
	}

}
