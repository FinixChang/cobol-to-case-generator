package com.csc.dpms.dr.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map; 

import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRTextExporter; 
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput; 
import net.sf.jasperreports.export.SimpleWriterExporterOutput; 

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.csc.dpms.app.base.BaseService;
import com.csc.dpms.core.annotation.Link;
import com.csc.dpms.dao.annotation.DataSource;
import com.csc.dpms.dao.service.SqlDao;
import com.csc.dpms.de.service.DEFileService;
import com.csc.dpms.de.util.DEResourceUtils;
import com.csc.dpms.report.ReportFormat;
import com.csc.dpms.report.exception.ReportException;
import com.csc.dpms.report.model.GenerateReportParameter; 

/**
 * AP透過ReportService來呼叫,依照輸入包裝好的報表參數GenerateReportParameter物件的參數設定內容,
 * 透過Jasper Report的lib來產製報表. 目前支援PDF|HTML|XML|XLSX|TXT|DOCX|CSV等格式.
 * @author i26048
 *
 */
@Service
public class DRGenerateReportService extends BaseService {
	@DataSource("udb.dbqsmf")
	SqlDao daoQSMF; // data source DBQSMF

	@DataSource("host.db2")
	SqlDao daoHOST; // data source HOST DB2

	@DataSource("udb.dbcsca")
	SqlDao daoCSCA; // data source DBCSCA

	@Link
	DEFileService deFileService;

	static Logger logger = LoggerFactory.getLogger(DRGenerateReportService.class);

	/**
	 * AP將產製報表之參數加入到GenerateReportParameter物件中帶入給generateReport()來產製報表.
	 * @param parameterObj
	 * @return 報表實體檔案物件
	 * @throws ReportException
	 */
	public File generateReport(GenerateReportParameter parameterObj) throws ReportException {
		File outputFile = null;
		try {
			// 執行共同步驟, 產出 jrprint 物件.
			String jasperPrint = generateJrprint(parameterObj);
			// 階段四 : 輸出 實體檔案
			String sysId = "".equals(parameterObj.getSysId()) ? 
					parameterObj.getJrxmlFileName().substring(0, 2).toLowerCase() : parameterObj.getSysId();
					
			//parameterObj.getJrxmlFileName().substring(0, 2).toLowerCase();
			String fileName = parameterObj.getJrxmlFileName().substring(0,
					parameterObj.getJrxmlFileName().lastIndexOf("."));
			fileName += Long.toString(System.currentTimeMillis());

			String outputFileFullPathName = deFileService.getPublicFilepath("dr", sysId, fileName);
			// 新增判斷目錄是否存在, 不存在的話就新增
			File targetDir = new File(outputFileFullPathName);
			if (!targetDir.getParentFile().exists()) {
				targetDir.getParentFile().mkdirs();
			}

			if (parameterObj.getFormat().equals(ReportFormat.PDF)) {
				logger.info("generateReport: Step4. 輸出  實體檔案: {}", outputFileFullPathName + ".pdf");
				JasperExportManager.exportReportToPdfFile(jasperPrint, outputFileFullPathName + ".pdf");
				outputFile = new File(outputFileFullPathName + ".pdf");
			} else if (parameterObj.getFormat().equals(ReportFormat.HTML)) {
				logger.info("generateReport: Step4. 輸出  實體檔案: {}", outputFileFullPathName + ".htm");
				JasperExportManager.exportReportToHtmlFile(jasperPrint, outputFileFullPathName + ".htm");
				outputFile = new File(outputFileFullPathName + ".htm");
			} else if (parameterObj.getFormat().equals(ReportFormat.XML)) {
				logger.info("generateReport: Step4. 輸出  實體檔案: {}", outputFileFullPathName + ".xml");
				JasperExportManager.exportReportToXmlFile(jasperPrint, outputFileFullPathName + ".xml", false);
				outputFile = new File(outputFileFullPathName + ".xml");
			} else if (parameterObj.getFormat().equals(ReportFormat.XLSX)) {
				logger.info("generateReport: Step4. 輸出  實體檔案: {}", outputFileFullPathName + ".xlsx");
				File jrprint = new File(jasperPrint);
				List<JasperPrint> jasperPrintList = new ArrayList<JasperPrint>();
				jasperPrintList.add((JasperPrint)JRLoader.loadObject(jrprint));  
				outputFile = new File(outputFileFullPathName + ".xlsx"); 

				JRXlsxExporter xlsxExporter = new JRXlsxExporter();
				xlsxExporter.setConfiguration(parameterObj.getXlsxReportConfiguration()); 
				xlsxExporter.setConfiguration(parameterObj.getXlsxExporterConfiguration());  
				xlsxExporter.setExporterInput(SimpleExporterInput.getInstance(jasperPrintList));
				xlsxExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputFile));
				xlsxExporter.exportReport();
			} else if (parameterObj.getFormat().equals(ReportFormat.CSV)) {
				logger.info("generateReport: Step4. 輸出  實體檔案: {}", outputFileFullPathName + ".csv");
				outputFile = new File(outputFileFullPathName + ".csv"); 

				JRCsvExporter exporter = new JRCsvExporter();
				exporter.setConfiguration(parameterObj.getCsvExporterConfiguration());
				exporter.setConfiguration(parameterObj.getCsvReportConfiguration());
				exporter.setExporterInput(new SimpleExporterInput(jasperPrint)); 
				exporter.setExporterOutput(new SimpleWriterExporterOutput(outputFile));
				exporter.exportReport(); 
			}else if (parameterObj.getFormat().equals(ReportFormat.TXT)) {
				logger.info("generateReport: Step4. 輸出  實體檔案: {}", outputFileFullPathName + ".txt");
				outputFile = new File(outputFileFullPathName + ".txt"); 

				JRTextExporter exporter = new JRTextExporter();
				exporter.setConfiguration(parameterObj.getTextExporterConfiguration());
				exporter.setConfiguration(parameterObj.getTextReportConfiguration());
				exporter.setExporterInput(new SimpleExporterInput(jasperPrint)); 
				exporter.setExporterOutput(new SimpleWriterExporterOutput(outputFile));
				exporter.exportReport();
			} else if (parameterObj.getFormat().equals(ReportFormat.DOCX)) {
				System.out.println("generateReport: Step4. 輸出  實體檔案: " +  outputFileFullPathName + ".docx");
				outputFile = new File(outputFileFullPathName + ".docx"); 

				JRDocxExporter exporter = new JRDocxExporter();
				exporter.setConfiguration(parameterObj.getDocxExporterConfiguration());
				exporter.setConfiguration(parameterObj.getDocxReportConfiguration());
				exporter.setExporterInput(new SimpleExporterInput(jasperPrint)); 
				exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputFile));
				exporter.exportReport();
			} else {
				logger.info("generateReport: Step4. 輸出  實體檔案失敗!! 格式不支援,目前僅支援[PDF|HTML|XML|XLSX|TXT|DOCX|CVS]");
				throw new ReportException("exportReportAsFile失敗: 格式不支援,目前僅支援[PDF|HTML|XML|XLSX|TXT|DOCX|CVS]");
			}
		} catch (Exception e) { 
			throw new ReportException("Failed to generate report, message:{}" + e.toString(), e);
		}
		logger.info("############### 產製報表作業結束 ###############");
		return outputFile;
	}

	/**
	 * AP將產製報表之參數加入到GenerateReportParameter物件中帶入給generateReport()來產製報表, 並將產製報表轉輸出到outputStream.
	 * @param parameterObj 報表實體檔案物件
	 * @param outputStream 輸出OutputStream物件
	 * @throws ReportException
	 */
	public void generateReport(GenerateReportParameter parameterObj, OutputStream outputStream) throws ReportException {
		File outputFile = generateReport(parameterObj);
		if (outputFile != null) {
			try {
				FileInputStream fileIn = new FileInputStream(outputFile);
				IOUtils.copy(fileIn, outputStream);
				fileIn.close();
				outputStream.flush();
			} catch (FileNotFoundException e) {
				// logger.error("Failed to generate report, message:{}", e.getMessage(), e);
				throw new ReportException("產製報表作業失敗: " + e.toString());
			} catch (IOException e) {
				// logger.error("Failed to generate report, message:{}", e.getMessage(), e);
				throw new ReportException("產製報表作業失敗: " + e.toString());
			} finally {
				IOUtils.closeQuietly(outputStream);
			}
		}
		logger.info("############### 報表OutputStream產製作業結束 ###############");
	}

	/**
	 * AP將產製報表之參數加入到GenerateReportParameter物件中帶入給generateReport()來產製報表, 並將產製報表轉輸出到httpServletResponse.
	 * @param parameterObj 報表實體檔案物件
	 * @param httpServletResponse 輸出httpServletResponse物件
	 * @throws ReportException
	 */
	public void generateReport(GenerateReportParameter parameterObj, HttpServletResponse httpServletResponse)
			throws ReportException {
		try {
			generateReport(parameterObj, httpServletResponse.getOutputStream());
		} catch (IOException e) {
			logger.error("Failed to generate report, message:{}", e.toString(), e);
		}
		logger.info("############### 報表OutputStream產製作業結束 ###############");
	}

	private String generateJrprint(GenerateReportParameter parameterObj) throws ReportException, JRException {
		String sysId = "".equals(parameterObj.getSysId()) ? 
				parameterObj.getJrxmlFileName().substring(0, 2).toLowerCase() : parameterObj.getSysId();
		logger.info("############### 產製報表作業開始 ###############");
		// 階段零 : 先將輸入之參數物件讀出並指定給全域變數
		initializeParameter(parameterObj);
		// 階段一 : Compile xml 檔 , 並產生 .jasper 中間檔
		logger.info("generateReport: Step1. Compile xml 檔並產生 .jasper 中間檔");
		xmlCompileToJasper(sysId,parameterObj.getJrxmlFileName(), false);

		// 階段二 : 填資料進 JRDataSource
		logger.info("generateReport: Step2. 填資料進 JRDataSource");
		JRDataSource jrDataSource = null;
		Collection<?> collection = parameterObj.getBeanCollection();
		if (collection != null && !collection.isEmpty()) {
			jrDataSource = new JRBeanCollectionDataSource(collection);
		} else {
			jrDataSource = new JREmptyDataSource(); 
		}
		//新增針對SubReport及CrossTab做處理的方法
		parameterObj.setCrossTableCollectionList();
		
		// 階段三 : 輸出 .jrprint 檔
		logger.info("generateReport: Step3. 輸出  .jrprint 檔");
		String jasperPrint = exportJrprint(sysId,parameterObj.getJrxmlFileName(), parameterObj.getFormat(),
				parameterObj.getParameter(), jrDataSource);
		logger.info("generateReport: Step3. 輸出  .jrprint 檔  {} 完成", jasperPrint);
		return jasperPrint;
	}

	private String exportJrprint(String sysId, String jrxmlFile, ReportFormat reportFormat, 
			Map<String, Object> reportRarameter,JRDataSource jrDataSource) throws ReportException, JRException {
		String fileName = jrxmlFile.substring(0, jrxmlFile.lastIndexOf(".")); 
		
		String jasperFileName = deFileService.getResourceFilepath(sysId, "dr", fileName + ".jasper");  
		String jasperPrint = deFileService.getResourceFilepath(sysId, "dr", fileName + ".jrprint"); 
		logger.info("JasperFillManager.fillReport() start");
		JasperFillManager.fillReportToFile(jasperFileName, jasperPrint, reportRarameter, jrDataSource);
		logger.info("JasperFillManager.fillReport() end"); 
		return jasperPrint;
	}

	private void initializeParameter(GenerateReportParameter parameterObj) throws ReportException {
		logger.info("############### 產製參數檢查開始 ###############");
		if (parameterObj == null) {
			logger.error("GenerateReportParameter 物件不存在");
			throw new ReportException("GenerateReportParameter 物件不存在");
		}
		if (parameterObj.getJrxmlFileName() == null) {
			logger.error("未指定 .jrxml 檔案名稱");
			throw new ReportException("未指定 .jrxml 檔案名稱");
		}
		String jrxmlName = parameterObj.getJrxmlFileName();
		if (jrxmlName.contains("\\")) {
			parameterObj.setJrxmlFileName(jrxmlName.substring(jrxmlName.lastIndexOf("\\") + 1));
		}
		if (jrxmlName.contains("/")) {
			parameterObj.setJrxmlFileName(jrxmlName.substring(jrxmlName.lastIndexOf("/") + 1));
		} 

		if (parameterObj.getParameter() == null) {
			parameterObj.setParameter(new HashMap<String, Object>()); 
		}
		if (parameterObj.getBeanCollection() == null) {
			parameterObj.setBeanCollection(new ArrayList<Object>()); 
		}
		logger.info("############### 產製參數檢查完成 ###############");
		logger.info("檔名: {}", parameterObj.getJrxmlFileName());
		logger.info("型態: {}", parameterObj.getFormat());
		logger.info("###########################################");
	}

	/**
	 * 將.jrxml檔案編譯成.jasper檔，如果 .jasper檔案不存在則編譯產生，如果存在則比較日期新舊，如果 XML 比較 新則會幫您編譯產生
	 * jasper isForce 是一個是否強制編譯的參數，如果設定為 true，則不論 jasper 檔案的日期皆會幫您編譯產生.jasper
	 * (產生的.jasper 檔案一定與 XML 檔案同一目錄下, 路徑設定請參考drConfig.xml中jrxmlRepository屬性)
	 * 
	 * @param jrxmlFilePath
	 *            - JasperReport 要用的 XML檔名不包含路徑 (eg.drtest.jrxml)
	 * @param isForce
	 *            - 是否強制編譯
	 * @return boolean - 傳回 編譯是否成功
	 * @throws JRException
	 */
	protected boolean xmlCompileToJasper(String sysId, String jrxmlFileName, boolean isForce) throws ReportException, JRException { 
		logger.info("sysId: {}", sysId);
		logger.info("jrxmlFileName: {}", jrxmlFileName);
		String jrxmlFilePath = deFileService.getResourceFilepath(sysId, "dr", jrxmlFileName);
		logger.info("jrxmlFilePath: {}", jrxmlFilePath);
		String jasperFilePath = jrxmlFilePath.substring(0, jrxmlFilePath.lastIndexOf(".")) + ".jasper";
		logger.info("jasperFilePath: {}", jasperFilePath);

		boolean isCompile = isForce;
		DEResourceUtils resourceUtils = DEResourceUtils.getInstance();
		boolean isAlwaysCompileXml = true;
		if (resourceUtils.getProperty("dr", "isAlwaysCompileXml") != null) {
			isAlwaysCompileXml = resourceUtils.getProperty("dr", "isAlwaysCompileXml").equals("true") ? true : false;
		}

		if (isAlwaysCompileXml) {
			logger.info("Compiling JRXML檔.....");
			JasperCompileManager.compileReportToFile(jrxmlFilePath, jasperFilePath);
			logger.info("Compiled JRXML檔.....");
			return true;
		} else if (!isCompile) { // 非強制，所以 判斷兩個檔案的日期，XML 比 jasper 新，就要編譯
			File jrxmlFile = new File(jrxmlFilePath);
			File jasperFile = new File(jasperFilePath);
			if (jasperFile.exists()) {
				if (jrxmlFile.lastModified() > jasperFile.lastModified()) {
					isCompile = true; // XML 比較新，所以編譯
				}
			} else {
				isCompile = true; // 不存在，所以一定要編譯
			}
		}
		if (isCompile) {
			logger.info("Compiling JRXML檔.....");
			// Compile xml 檔 , 並產生 .jasper 中間檔
			JasperCompileManager.compileReportToFile(jrxmlFilePath, jasperFilePath);
			logger.info("Compiled JRXML檔.....");
		}
		return true;
	}
}