//不要寫任何 package,否則 classloader 會載入不了
//若同時要用 notepad++ 及 eclipse 編輯本檔, 請用 UTF8 編碼
import java.lang.reflect.Method;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import com.csc.dpms.da.ObjectCreator;
import com.csc.dpms.da.source.vo.Column;
import com.csc.dpms.da.source.vo.Enum;
import com.csc.dpms.da.source.vo.GeneratorConfiguration;

import com.beust.jcommander.internal.Lists;
import com.csc.dpms.da.util.ParameterUtil;

public class MyClass {
	/**
	 * 
	 * @param config
	 * @return
	 */
	public List<String> getEnumSpecDescription(GeneratorConfiguration config, String fileName) {
		List<String> result = Lists.newArrayList();
		for (Column column : config.getTable().getEnumColumn()) {
			//for spec description
			String enumDesc = "";
			for (Enum en : column.getEnums()) {
    			enumDesc = String.format("%s[%s:%s]", enumDesc , en.getValue(), en.getName());
			}
			result.add(String.format("%s:%s", column.getName(), enumDesc));
		}
		return result;
	}
	
	/**
	 * 
	 * @param config
	 * @return
	 */
	public List<String> getEnumColumnClassName(GeneratorConfiguration config, String fileName) {
		List<String> result = Lists.newArrayList();
		for (Column column : config.getTable().getEnumColumn()) {
			//JOBTYPE => DQWJobTypeName
			result.add(column.getEnumClassName());
		}
		return result;
	}
	
	/**
	 * 
	 * @param config
	 * @return
	 */
	public List<String> getSearchPropertyList(GeneratorConfiguration config, String fileName) {
		String[] searchArray = config.getTable().getSearch().split(",");
		List<String> result = Lists.newArrayList();
		Method getProperty = null;
		try {
			getProperty = Column.class.getMethod("getProperty");
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//property
		for (String columnId : searchArray) {
			result.add(Column.getValueBy(config.getTable(), getProperty, columnId));
		}
		
		return result;
	}
	
	
	
	public List<String> getSqlLikeList(GeneratorConfiguration config, String fileName) {
		List<String> result = Lists.newArrayList();
		String[] searchIdArray = config.getTable().getSearch().split(",");
		List<String> searchPropertyList = getSearchPropertyList(config, fileName); 
		
		int i = 0;
		String sqlLike = "";
		
		for (String searchId : searchIdArray) {
			if (i == 0){
				result.add("and (");
				sqlLike = String.format("      LOWER(%s) like #{%s,jdbcType=CHAR}", searchId, searchPropertyList.get(0));
			}else{
				sqlLike = String.format("   or LOWER(%s) like #{%s,jdbcType=CHAR}", searchId, searchPropertyList.get(i));
			}
			result.add(sqlLike);
			i++;
		}
		if (searchIdArray.length > 0){
			result.add(")");
		}
		return result;
	}
	
	
	public List<String> getThisEnumDefine(GeneratorConfiguration config, String fileName) {
		List<String> result = Lists.newArrayList();
		for (Column column : config.getTable().getEnumColumn()) {
			if (fileName.contains(column.getEnumClassName())) {
				for (Enum en : column.getEnums()) {
					result.add(String.format("%s(\"%s\", \"%s\")", en.getProperty(), en.getValue(), en.getName()));
				}
				return result;
			}
		}
		return result;
	}
	
	
	public List<String> getAllColumnId(GeneratorConfiguration config, String fileName) {
		List<String> result = Lists.newArrayList();
		for (Column column : config.getTable().getColumnList()) {
			result.add(String.format("%s", column.getId()));
		}
		return result;
	}

	public List<String> getEnumVarAndClassName(GeneratorConfiguration config, String fileName) {
		List<String> result = Lists.newArrayList();
		for (Column column : config.getTable().getEnumColumn()) {
			result.add(String.format("%s", column.getEnumVarAndClassName()));
		}
		return result;
	}
	
	public List<String> getFieldHeaderList(GeneratorConfiguration config, String fileName) {
		List<String> result = Lists.newArrayList();
		for (Column column : config.getTable().getColumnList()) {
			String addName = column.getEnums()==null ? "" : "Name";
			String addDataKey = "true".equals(column.getPrimary()) ? " data-key=\"me\"" : "";
			String addDataFormatter = config.getTable().getSearch().contains(column.getId()) ? " data-formatter=\"searchFormatter\"" : "";
			
			String fieldHeader = String.format("\"%s%s\"%s%s  data-valign=\"middle\">%s", column.getProperty(),
					addName, addDataKey, addDataFormatter, column.getName());
			result.add(fieldHeader);
		}
		return result;
	}
	
	public List<String> getColumnToDivList(GeneratorConfiguration config, String fileName) {
		List<String> result = Lists.newArrayList();
		String line = "";
		for (Column column : config.getTable().getColumnList()) {
			StringBuilder sb = new StringBuilder();
			sb.append("        <div class=\"form-group\">\n");

			line = String.format("            <label for=\"%s.%s\" class=\"pidAvd1er col-sm-1 control-label\">%s</label>\n"
					, config.getTable().getVoVar(), column.getProperty(), column.getName());
			sb.append(line);


			line = String.format("            <div class=\"col-sm-10\">\n");
			sb.append(line);
			if (column.getEnums()==null){
				String addDataKey = "true".equals(column.getPrimary()) ? String.format(" data-key=\"${%s.%s}\"",config.getTable().getVoVar(), column.getProperty()) : "";
				line = String.format("                <input type=\"%s\" name=\"%s.%s\" %s value=\"${%s.%s}\" class=\"form-control\">\n"
						, column.getInputType(), config.getTable().getVoVar(), column.getProperty(), addDataKey, config.getTable().getVoVar(), column.getProperty());
				sb.append(line);
			}else{
				if ("radio".equals(column.getInputType())){
					for (Enum display : column.getEnums()) {
						line = String.format("                <input type=\"%s\" name=\"%s.%s\"  value=\"%s\" <c:if test=\"${%s.%s == '%s'}\">checked</c:if> >\n"
								, column.getInputType(), config.getTable().getVoVar(), column.getProperty(), display.getValue(), config.getTable().getVoVar(), column.getProperty(), display.getValue());
						sb.append(line);
						
						line = String.format("                                               %s\n" , display.getName());
						sb.append(line);
					}
				}else{
					//
					line = String.format("                <!-- <%s> not implement!! -->\n",column.getInputType());
					sb.append(line);
				}
				
			}
			
			line = String.format("            </div>\n");
			sb.append(line);

			sb.append("        </div>\n");
			
			
			result.add(sb.toString());
		}
		return result;
	}
	
	public List<String> getExtendVoEnumGetNameList(GeneratorConfiguration config, String fileName) {
		List<String> result = Lists.newArrayList();
		
		
		for (Column column : config.getTable().getEnumColumn()) {
//		public String getNotificationMethodName(){
//			return DQWNotificationMethodName.getNameByValue(this.getNotificationMethod());
//		}		
			String classGetName = String.format("    public String %sName(){\n", ParameterUtil.toCascade("get", column.getProperty()));
			
			classGetName = String.format("%s        return %s.getNameByValue(this.%s());\n", classGetName, column.getEnumClassName(), ParameterUtil.toCascade("get", column.getProperty()));
			
			classGetName = String.format("%s    }", classGetName);
			result.add(classGetName);
		}
		return result;
		//return extendVoDisplayGetNameList;
	}	
	
	
	
	
}